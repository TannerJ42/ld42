﻿Shader "Hidden/ColorChanger"
{
	Properties
	{
		[PerRendererData] _MainTex ("Base (RGB)", 2D) = "white" {}
		_ColorFrom1 ("Color From 1", Color) = (1, 1, 1)
		_ColorFrom2 ("Color From 2", Color) = (0.576, 0.584, 0.596)
		_ColorFrom3 ("Color From 3", Color) = (0.576, 0.584, 0.596)

		_ColorTo1 ("Color To 1", Color) = (1, 0, 0)
		_ColorTo2 ("Color To 2", Color) = (1, 1, 0)
		_ColorTo3 ("Color To 3", Color) = (1, 1, 1)

		_Tolerance ("Tolerance", float) = 0.1

		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0

	}
    SubShader
    {
        Tags
        {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
        }

        Pass
        {
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
		Cull Off

        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        #pragma multi_compile DUMMY PIXELSNAP_ON

        sampler2D _MainTex;
        float4 _ColorFrom1;
        float4 _ColorTo1;

        float4 _ColorFrom2;
        float4 _ColorTo2;

        float4 _ColorFrom3;
        float4 _ColorTo3;

		float _Tolerance;
		
        struct Vertex
        {
            float4 vertex : POSITION;
            float2 uv_MainTex : TEXCOORD0;
            float2 uv2 : TEXCOORD1;
        };

        struct Fragment
        {
            float4 vertex : POSITION;
            float2 uv_MainTex : TEXCOORD0;
            float2 uv2 : TEXCOORD1;
        };

        Fragment vert(Vertex v)
        {
            Fragment o;

            o.vertex = UnityObjectToClipPos(v.vertex);
            o.uv_MainTex = v.uv_MainTex;
            o.uv2 = v.uv2;

            return o;
        }

        float4 frag(Fragment IN) : COLOR
        {
            half4 c = tex2D(_MainTex, IN.uv_MainTex);
			if (c.a != 1)
			{
				return c;
			}
            if (c.r >= _ColorFrom1.r - _Tolerance && c.r <= _ColorFrom1.r + _Tolerance
                && c.g >= _ColorFrom1.g - _Tolerance && c.g <= _ColorFrom1.g + _Tolerance
                    && c.b >= _ColorFrom1.b - _Tolerance && c.b <= _ColorFrom1.b + _Tolerance)
            {
                return _ColorTo1;
            }

            if (c.r >= _ColorFrom2.r - _Tolerance && c.r <= _ColorFrom2.r + _Tolerance
                && c.g >= _ColorFrom2.g - _Tolerance && c.g <= _ColorFrom2.g + _Tolerance
                    && c.b >= _ColorFrom2.b - _Tolerance && c.b <= _ColorFrom2.b + _Tolerance)
            {
                return _ColorTo2;
            }

            if (c.r >= _ColorFrom3.r - _Tolerance && c.r <= _ColorFrom3.r + _Tolerance
                && c.g >= _ColorFrom3.g - _Tolerance && c.g <= _ColorFrom3.g + _Tolerance
                    && c.b >= _ColorFrom3.b - _Tolerance && c.b <= _ColorFrom3.b + _Tolerance)
            {
                return _ColorTo3;
            }

            return c;
        }
            ENDCG
        }
    }
} 