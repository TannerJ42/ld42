﻿using UnityEngine;
using System.Collections;

public class PlatformController : MonoBehaviour
{
    private float maxTime = 60;
    [SerializeField] Sprite[] sprites = null;

    void Start()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = sprites[Random.Range(0, sprites.Length)];

        float timeMultiplier = Mathf.Abs(transform.position.x);
        float timeTillFall = maxTime - timeMultiplier;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "platformManager")
        {
            GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
    }
}
