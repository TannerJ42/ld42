﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour {
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "fallingPlatform")
        {
            GetComponent<BoxCollider2D>().isTrigger = false;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
    }
}
