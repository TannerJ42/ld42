﻿using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour
{
    [SerializeField] int RoundsToWin = 2;

    [SerializeField] FighterModel leftFighter = null;
    [SerializeField] FighterModel rightFighter = null;

    private int leftWins;
    private int rightWins;

    private bool hasEnded = false;

    [SerializeField] Transform deathYObject = null;

    [SerializeField] ColorChooser colorChooser = null;

    [SerializeField] CameraController cameraController = null;

    [SerializeField] GameObject[] leftWinDots;
    [SerializeField] GameObject[] rightWinDots;

    private FighterModel winner = null;
    private FighterModel loser = null;

    [SerializeField] GameObject roundText;
    [SerializeField] GameObject fightText;

    [SerializeField] Announcer announcer;

    private void Start()
    {
        cameraController.Init(leftFighter, rightFighter);

        if (PlayerPrefs.HasKey("MatchInProgress"))
        {
            PlayerPrefs.DeleteKey("MatchInProgress");
            leftWins = PlayerPrefs.GetInt("LeftWins");
            rightWins = PlayerPrefs.GetInt("RightWins");
            announcer.SetPack(PlayerPrefs.GetString("Announcer"));
        }
        else
        {
            colorChooser.RandomizeEnemyColor();
            string ann = announcer.GetRandomPackName();
            PlayerPrefs.SetString("Announcer", ann);
            announcer.SetPack(ann);
        }

        leftFighter.SetColorChoice(colorChooser.PlayerColor);
        rightFighter.SetColorChoice(colorChooser.EnemyColor);

        DrawDots();

        float deathY = deathYObject.transform.position.y;
        leftFighter.Init(this, deathY);
        rightFighter.Init(this, deathY);

        // difficulty
        int difficulty = 0;
        if (PlayerPrefs.HasKey("diff"))
        {
            difficulty = PlayerPrefs.GetInt("diff");
        }

        switch (difficulty)
        {
            case 0:
                break;
            case 1:
                rightFighter.IncreaseMass(5);
                break;
            case 2:
                rightFighter.IncreaseMass(10);
                break;
        }

        StartCoroutine(StartFight());
    }

    private void DrawDots()
    {
        for (int i = 0; i < leftWinDots.Length; i++)
        {
            leftWinDots[i].GetComponent<Image>().color = colorChooser.PlayerColor.color;
            leftWinDots[i].SetActive(i <= leftWins - 1);
        }

        for (int i = 0; i < rightWinDots.Length; i++)
        {
            rightWinDots[i].GetComponent<Image>().color = colorChooser.EnemyColor.color;
            rightWinDots[i].SetActive(i <= rightWins - 1);
        }
    }

    void WritePrefs()
    {
        PlayerPrefs.SetString("MatchInProgress", "OK");
        PlayerPrefs.SetInt("LeftWins", leftWins);
        PlayerPrefs.SetInt("RightWins", rightWins);
    }

    public void OnFighterLose(FighterModel fighterModel)
    {
        if (hasEnded)
        {
            return;
        }
        hasEnded = true;
        loser = fighterModel;
        if (loser == leftFighter)
        {
            winner = rightFighter;
            rightWins++;
        }
        else
        {
            winner = leftFighter;
            leftWins++;
        }
        DrawDots();
        StartCoroutine(Win());
    }

    private IEnumerator StartFight()
    {
        int roundNumber = leftWins + rightWins + 1;
        switch (roundNumber)
        {
            case 1:
                announcer.Announce(Announcer.AnnouncerClip.Round1);
                break;
            case 2:
                announcer.Announce(Announcer.AnnouncerClip.Round2);
                break;
            case 3:
                announcer.Announce(Announcer.AnnouncerClip.Round3);
                break;
        }
        roundText.gameObject.SetActive(true);
        roundText.GetComponent<Text>().text = "ROUND " + roundNumber;
        yield return new WaitForSeconds(1.5f);
        roundText.SetActive(false);

        yield return new WaitForSeconds(0.5f);
        announcer.Announce(Announcer.AnnouncerClip.Fight);
        fightText.SetActive(true);
        leftFighter.StartFight();
        rightFighter.StartFight();
    }

    private IEnumerator Win()
    {
        cameraController.RemoveFighter(loser);
        if (leftFighter == winner)
        {
            announcer.Announce(Announcer.AnnouncerClip.Victory);
        }
        else
        {
            announcer.Announce(Announcer.AnnouncerClip.Defeat);
        }
        yield return new WaitForSeconds(1.0f);
        if (leftWins >= RoundsToWin)
        {
            SceneManager.LoadScene("PostFight");
        }
        else if (rightWins >= RoundsToWin)
        {
            PlayerPrefs.SetInt("defeat", 42);
            SceneManager.LoadScene("PostFight-Defeat");
        }
        else
        {
            WritePrefs();
            SceneManager.LoadScene("Fight");
        }
    }
#if UNITY_EDITOR
    [MenuItem("Debug/Clear Save Data")]
    public static void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }
#endif
}
