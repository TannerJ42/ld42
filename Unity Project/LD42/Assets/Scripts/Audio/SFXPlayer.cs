﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    AudioSource[] audioSources;

    private void Awake()
    {
        audioSources = new AudioSource[5];
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i] = gameObject.AddComponent<AudioSource>();
        }
    }

    [SerializeField] AudioClip[] PunchSounds;
    [SerializeField] AudioClip[] GetHitSounds;
    [SerializeField] AudioClip[] JumpSounds;

    private AudioSource getFreeSource()
    {
        foreach (var source in audioSources)
        {
            if (!source.isPlaying)
            {
                return source;
            }
        }
        return null;
    }

    private void PlayClip(AudioClip clip)
    {
        AudioSource source = getFreeSource();
        if (source == null)
        {
            return;
        }
        source.clip = clip;
        source.Play();
    }

    public void PlayThrowPunchAudio()
    {
        PlayClip(PunchSounds[Random.Range(0, PunchSounds.Length - 1)]);
    }

    public void PlayGetHitAudio()
    {
        PlayClip(GetHitSounds[Random.Range(0, GetHitSounds.Length - 1)]);
    }

    public void PlayJumpAudio()
    {
        PlayClip(JumpSounds[Random.Range(0, JumpSounds.Length - 1)]);
    }

}
