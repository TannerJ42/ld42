﻿using UnityEngine;
using System.Collections;
using System;

public class Announcer : MonoBehaviour
{
    [SerializeField] AudioSource source;

    [Serializable]
    public class VFXPack
    {
        public string name;
        public AudioClip fight;
        public AudioClip victory;
        public AudioClip defeat;
        public AudioClip[] rounds;
    }

    public enum AnnouncerClip
    {
        Fight,
        Victory,
        Defeat,
        Round1,
        Round2,
        Round3
    }
    private VFXPack pack;
    [SerializeField] VFXPack[] packs;

    public string GetRandomPackName()
    {
        return packs[UnityEngine.Random.Range(0, packs.Length)].name;
    }

    public void SetPack(string name)
    {
        foreach (var pack in packs)
        {
            if (pack.name == name)
            {
                this.pack = pack;
                return;
            }
        }
    }

    public void Announce(AnnouncerClip clip)
    {
        switch (clip)
        {
            case AnnouncerClip.Fight:
                PlayClip(pack.fight);
                break;
            case AnnouncerClip.Victory:
                PlayClip(pack.victory);
                break;
            case AnnouncerClip.Defeat:
                PlayClip(pack.defeat);
                break;
            case AnnouncerClip.Round1:
                PlayClip(pack.rounds[0]);
                break;
            case AnnouncerClip.Round2:
                PlayClip(pack.rounds[1]);
                break;
            case AnnouncerClip.Round3:
                PlayClip(pack.rounds[2]);
                break;
        }
    }

    private void PlayClip(AudioClip clip)
    {
        if (source.isPlaying)
        {
            source.Stop();
        }

        source.clip = clip;
        source.Play();
    }
}
