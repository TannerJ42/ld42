﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGMManager : MonoBehaviour
{
    private AudioSource audioSource;

    [Serializable]
    public class SceneBGM
    {
        public int SceneID;
        public AudioClip Clip;
    }

    [SerializeField] SceneBGM[] Scenes;
    [SerializeField] AudioClip DefaultBGM;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        BGMManager[] managers = FindObjectsOfType<BGMManager>();
        if (managers.Length > 1)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelLoaded;
    }

    private void OnLevelLoaded(Scene scene, LoadSceneMode load)
    {
        AudioClip clip = DefaultBGM;
        foreach(var scenebgm in Scenes)
        {
            if (scene.buildIndex == scenebgm.SceneID)
            {
                clip = scenebgm.Clip;
            }
        }

        if (clip == null)
        {
            audioSource.Stop();
            audioSource.clip = null;
        }
        else if (clip != audioSource.clip)
        {
            audioSource.Stop();
            audioSource.clip = clip;
            audioSource.Play();
        }

    }
}
