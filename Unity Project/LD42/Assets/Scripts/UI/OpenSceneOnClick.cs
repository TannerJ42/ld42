﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenSceneOnClick : MonoBehaviour
{

    public Scenes Scene;
#if (UNITY_EDITOR)
    [MenuItem("Quibbles/Start From Title")]
    static void LaunchFromTitle()
    {
        LaunchScene(Scenes.Title);
    }

    [MenuItem("Quibbles/Start From Main")]
    static void LaunchFromMain()
    {
        LaunchScene(Scenes.Main);
    }

    [MenuItem("Quibbles/Start From Fight")]
    static void LaunchFromFight()
    {
        LaunchScene(Scenes.Fight);
    }
#endif
    static void LaunchScene(Scenes name)
    {
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
        {
            SceneManager.LoadScene(name.ToString());
        }
        else
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/Scenes/" + name.ToString() + ".unity");
            EditorApplication.isPlaying = true;
        }
#else
        SceneManager.LoadScene(name.ToString());
#endif
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(Scene.ToString());
    }
}