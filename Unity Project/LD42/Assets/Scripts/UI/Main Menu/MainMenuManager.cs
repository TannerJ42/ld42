﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public void Fight()
    {
        if (!PlayerPrefs.HasKey("SeenTutorial"))
        {
            PlayerPrefs.SetString("SeenTutorial", "42");
            PlayerPrefs.SetString("Tut1", "ok");
            SceneManager.LoadScene("HowTo");
        }
        else
        {
            SceneManager.LoadScene("Fight");
        }
    }

    private string[] diffs = new string[3] { "EASY", "MID", "HARD" };

    private int diffIndex = 0;

    public Text diffText;

    public void Start()
    {
        if (PlayerPrefs.HasKey("diff"))
        {
            diffIndex = PlayerPrefs.GetInt("diff");
        }
        SetDiff();
    }

    public void DecreaseDiff()
    {
        diffIndex--;
        if (diffIndex < 0)
        {
            diffIndex = diffs.Length - 1;
        }
        SetDiff();
    }

    public void IncreaseDiff()
    {
        diffIndex++;
        if (diffIndex == diffs.Length)
        {
            diffIndex = 0;
        }
        SetDiff();
    }

    void SetDiff()
    {
        diffText.text = diffs[diffIndex];
        PlayerPrefs.SetInt("diff", diffIndex);
    }
}
