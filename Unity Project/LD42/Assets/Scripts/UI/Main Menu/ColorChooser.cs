﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChooser : MonoBehaviour
{
    [SerializeField] ColorChoice[] choices = null;
    [SerializeField] SpriteRenderer spriteRenderer = null;
    private int currentChoice = 0;
    private int enemyChoice = 1;

    public ColorChoice PlayerColor
    {
        get
        {
            return choices[currentChoice];
        }
    }

    public ColorChoice EnemyColor
    {
        get
        {
            return choices[enemyChoice];
        }
    }

	void Awake()
    {
        if (PlayerPrefs.HasKey("PlayerIndex"))
        {
            currentChoice = PlayerPrefs.GetInt("PlayerIndex");
        }
        if (PlayerPrefs.HasKey("EnemyIndex"))
        {
            enemyChoice = PlayerPrefs.GetInt("EnemyIndex");
        }

        Choose();
	}
	
    void Choose()
    {
        if (spriteRenderer != null)
        {
            spriteRenderer.material = choices[currentChoice].material;
        }

    }

    public void RandomizeEnemyColor()
    {
        enemyChoice = GetRandomColorChoice();
        while (enemyChoice == currentChoice)
        {
            enemyChoice = GetRandomColorChoice();
        }
        SavePrefs();
    }

    int GetRandomColorChoice()
    {
        return Random.Range(0, choices.Length - 1);
    }

    public void Prev()
    {
        currentChoice--;
        if (currentChoice == -1)
        {
            currentChoice = choices.Length - 1;
        }
        Choose();
        SavePrefs();
    }

    public void Next()
    {
        currentChoice++;
        if (currentChoice == choices.Length)
        {
            currentChoice = 0;
        }
        Choose();
        SavePrefs();
    }

    void SavePrefs()
    {
        PlayerPrefs.SetInt("PlayerIndex", currentChoice);
        PlayerPrefs.SetInt("EnemyIndex", enemyChoice);
    }
}
