﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ColorChoice
{
    public Color color;
    public Material material;
}
