﻿using UnityEngine;
using UnityEditor;

public enum Scenes
{
    Title,
    Main,
    Fight,
    Credits,
    Options,
    Stats,
    LevelSelect,
    PostFight,
    HowTo,
    Crawl
}