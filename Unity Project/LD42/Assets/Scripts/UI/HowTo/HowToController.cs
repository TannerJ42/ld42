﻿using UnityEngine;
using System.Collections;

public class HowToController : MonoBehaviour
{
    public GameObject FightButton;
    public GameObject BackButton;


    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.HasKey("Tut1")) // got here through the fight button
        {
            PlayerPrefs.DeleteKey("Tut1");
            PlayerPrefs.SetString("Tut2", "42");
            FightButton.SetActive(true);
            BackButton.SetActive(false);
        }
        else // got here from the main menu
        {
            FightButton.SetActive(false);
            BackButton.SetActive(true);
        }

    }
}
