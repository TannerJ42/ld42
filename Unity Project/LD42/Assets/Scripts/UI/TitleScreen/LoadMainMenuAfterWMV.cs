﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class LoadMainMenuAfterWMV : MonoBehaviour {

    private VideoPlayer videoPlayer;
    private bool playing = false;
    [SerializeField] float secondsBeforePlaying = 1.0f;

	void Start ()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }

    private void Awake()
    {
        StartCoroutine(PlayMovie());
    }

    private IEnumerator PlayMovie()
    {
        yield return new WaitForSecondsRealtime(secondsBeforePlaying);
        videoPlayer.Play();
        playing = true;
    }

    // Update is called once per frame
    void Update()
    {
		if (playing)
        {
            if (!videoPlayer.isPlaying || Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Main");
            }
        }
	}
}
