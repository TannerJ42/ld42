﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
    List<FighterModel> fighters;

    //[SerializeField] Transform upperBound;
    //[SerializeField] Transform lowerBound;
    //[SerializeField] Transform rightBound;
    //[SerializeField] Transform leftBound;

    private Vector3 targetPosition;

    private Camera mainCamera;

    private float moveSpeed = 3.5f;
    private float zoomSpeed = 1.0f;

    [SerializeField] private float maxOrth = 10;
    [SerializeField] private float minOrth = 5;
    [SerializeField] private float distanceUpperBound = 30.0f;
    [SerializeField] private float distanceLowerBound = 0.0f;

    private float orthBuffer;

    private float orthRatio;
    private float distRatio;

    float orthSize;

    void Awake()
    {
        mainCamera = GetComponent<Camera>();
    }

    private void Start()
    {
        orthRatio = (maxOrth - minOrth) / 100;
        distRatio = (distanceUpperBound - distanceLowerBound) / 100;
    }

    public void Init(FighterModel leftFighter, FighterModel rightFighter)
    {
        fighters = new List<FighterModel>();
        fighters.Add(leftFighter);
        fighters.Add(rightFighter);
    }

    void Update()
    {
        Move();
        Zoom();
    }

    private void Move()
    {
        Vector3 center = Vector3.zero;

        foreach (FighterModel fighter in fighters)
        {
            center += fighter.transform.position;
        }
        targetPosition = center / fighters.Count;
        targetPosition.z = mainCamera.transform.position.z;

        Vector3 newPosition = Vector3.Lerp(mainCamera.transform.position, targetPosition, moveSpeed * Time.deltaTime);
        mainCamera.transform.position = newPosition;
    }

    private void Zoom()
    {
        orthSize = maxOrth;

        float maxDist = GetMaxDistance();

        if (maxDist > distanceUpperBound)
        {
            orthSize = maxOrth;
        }
        else if (maxDist < distanceLowerBound)
        {
            orthSize = minOrth;
        }
        else
        {
            float difference = (maxDist - distanceLowerBound) / distRatio;
            float orthDiff = difference * orthRatio;
            orthSize = minOrth + orthDiff + orthBuffer;
        }

        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthSize, zoomSpeed * Time.deltaTime);
    }

    private float GetMaxDistance()
    {
        float maxDist = 0f;
        for (int i = 0; i < fighters.Count; i++)
        {
            for (int j = 0; j < fighters.Count; j++)
            {
                if (i == j)
                {
                    continue;
                }
                float dist = Vector3.Distance(fighters[i].transform.position, fighters[j].transform.position);
                if (dist > maxDist)
                {
                    maxDist = dist;
                }
            }
        }
        return maxDist;
    }

    public void RemoveFighter(FighterModel fighter)
    {
        fighters.Remove(fighter);
    }
}
