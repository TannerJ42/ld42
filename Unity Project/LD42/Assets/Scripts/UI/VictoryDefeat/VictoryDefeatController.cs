﻿using UnityEngine;
using System.Collections;

public class VictoryDefeatController : MonoBehaviour
{
    [SerializeField] ColorChooser colorChooser;
    [SerializeField] Animator anim;
    [SerializeField] SpriteRenderer spriteRenderer;

    public bool victory = false;

    public void Awake()
    {
    }

    public void Start()
    {
        ColorChoice color = colorChooser.PlayerColor;
        Material mat = color.material;
        spriteRenderer.material = mat;

        anim.SetTrigger(victory ? "victory" : "defeat");
    }

}
