﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Crawl : MonoBehaviour {

    public Animation anim;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            Skip();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Skip();
        }

        if (!anim.isPlaying)
        {
            Skip();
        }
	}

    void Skip()
    {
        if (PlayerPrefs.HasKey("Tut2"))
        {
            PlayerPrefs.DeleteKey("Tut2");
            SceneManager.LoadScene("Fight");
        }
        else
        {
            SceneManager.LoadScene("Main");
        }

    }
}
