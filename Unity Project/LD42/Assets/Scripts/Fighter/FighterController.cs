﻿using UnityEngine;

public class FighterController : FighterControllerBase
{
    public FighterModel model;

    public override bool IsHoldingDown
    {
        get
        {
            return Input.GetAxis("Vertical") < 0;
        }

    }

    void Update()
    {
        float xMove = Input.GetAxis("Horizontal");
        model.TryMove(xMove);

        if (Input.GetAxis("Jump") > 0)
        {
            model.TryStartJump();
        }
        else
        {
            model.TryStopJump();
        }

        if (Input.GetAxis("Attack") > 0)
        {
            model.TryAttack();
        }
    }
}
