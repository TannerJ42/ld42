﻿using System;
using UnityEngine;

public class FighterView : MonoBehaviour
{
    [SerializeField] SpriteRenderer headRenderer = null;
    [SerializeField] ParticleSystem getHitParticles = null;
    [SerializeField] ParticleSystem deathParticles = null;
    [SerializeField] ParticleSystem beamParticles = null;


    bool flipped;
    Animator anim;

    [SerializeField] AudioSource pickupAudio = null;

	void Start()
    {
        anim = GetComponent<Animator>();
        flipped = transform.parent.localScale.x < 0;
	}

    public void SetColor(Color color)
    {
        var settings = getHitParticles.main;
        settings.startColor = color;

        settings = deathParticles.main;
        settings.startColor = color;

        settings = beamParticles.main;
        settings.startColor = color;
    }

    public void Jump()
    {
        anim.SetTrigger("jump");
    }

    public void GetHit()
    {
        anim.SetTrigger("getHit");
        getHitParticles.Play();
    }

    public void Punch()
    {
        anim.SetTrigger("punch");
    }

    public void Uppercut()
    {
        anim.SetTrigger("uppercut");
    }

    public void Airslam()
    {
        anim.SetTrigger("airslam");
    }

    public void StartBlock()
    {
        anim.SetBool("isBlocking", true);
    }

    public void EndBlock()
    {
        anim.SetBool("isBlocking", false);
    }

    public void Pickup()
    {
        if (pickupAudio.isPlaying)
        {
            pickupAudio.Stop();
        }
        pickupAudio.Play();
    }

    public void SetVelocity(float xVelocity)
    {
        anim.SetFloat("xVelocity", Mathf.Abs(xVelocity));
        if (xVelocity < 0 && !flipped)
        {
            Flip();
        }
        else if (xVelocity > 0 && flipped)
        {
            Flip();
        }
    }

    private void Flip()
    {
        flipped = !flipped;
        Vector3 scale = transform.localScale;
        scale.x = -scale.x;
        transform.localScale = scale;
    }

    public void SetMaterial(Material material)
    {
        headRenderer.material = material;
    }

    public void Super(bool enabled)
    {
        if (enabled)
        {
            beamParticles.Play();
        }
        else
        {
            beamParticles.Stop();
        }

    }

    public void Die()
    {
        deathParticles.Play();
    }
}
