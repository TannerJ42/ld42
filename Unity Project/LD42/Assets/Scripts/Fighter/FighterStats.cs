﻿using UnityEngine;

public class FighterStats : MonoBehaviour
{
    [Header("Movement")]
    public float MoveSpeed = 20.0f;
    public float MaxMoveSpeed = 80.0f;

    [Header("Movement")]
    public float JumpForce = 400.0f;
    
    [Header("Punch")]
    public float PunchCooldown = 1.0f;
    public Vector2 PunchVelocity = new Vector2(45, 45);
    public int PunchDamage = 1;

    [Header("Airslam")]
    public float AirslamCooldown = 3.0f;
    public Vector2 AirslamVelocity = new Vector2(45, 45);
    public int AirslamDamage = 2;

    [Header("Uppercut")]
    public float UppercutCooldown = 1.0f;
    public Vector2 UppercutVelocity = new Vector2(90, 90);
    public int UppercutDamage = 3;

    [Header("Mass")]
    public float MinScale = 0.5f;
    public float MaxScale = 1.5f;
    public float MinMass = 0.5f;
    public float MaxMass = 1.5f;
    public float ScaleIncrementSize = 0.05f;
    public float MassIncrementSize = 0.05f;

    [Header("Super")]
    public float SuperThreshold = 1.5f;
    public float SuperBonus = 50;

    [Header("Stun")]
    public float StunTime = 0.1f;
}
