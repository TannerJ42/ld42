﻿using System;
using UnityEngine;

public class FighterModel : MonoBehaviour
{
    FighterStats stats;
    FighterView view;
    Rigidbody2D rigidbody2d;

    private bool grounded = false;

    public Transform scaleTransform;

    public LayerMask groundLayer;
    public Transform groundedTopLeft;
    public Transform groundedBottomRight;

    public TrailRenderer trailRenderer;

    private bool canDoubleJump = false;
    private bool hasDoubleJumped = false;
    private bool jumping = false;
    private bool holdingJump = false;

    private float airslamAvailableTime = 0;
    private float uppercutAvailableTime = 0;
    private float punchAvailableTime = 0;

    private int currentAttackDecrements = 0;

    private Vector2 attackVelocity;

    public LayerMask enemyLayer;

    private GameplayController gameplayController;
    private float deathY;

    private ColorChoice color;

    public GameObject pickupPrefab = null;

    public FighterControllerBase myController = null;
    private bool super = false;

    private float recoverTime = 0;
    private bool stunned
    {
        get
        {
            return recoverTime >= Time.timeSinceLevelLoad;
        }
    }

    public void Init(GameplayController controller, float deathY)
    {
        myController.enabled = false;
        this.deathY = deathY;
        this.gameplayController = controller;
    }

    public void StartFight()
    {
        myController.enabled = true;
    }

    void Start()
    {
        view = GetComponentInChildren<FighterView>();
        stats = GetComponent<FighterStats>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    public void SetColorChoice(ColorChoice playerColor)
    {
        color = playerColor;
        view.SetMaterial(playerColor.material);
        view.SetColor(playerColor.color);
        trailRenderer.startColor = playerColor.color;
        trailRenderer.endColor = playerColor.color;
    }

    private void Update()
    {
        grounded = Physics2D.OverlapArea(groundedTopLeft.position, groundedBottomRight.position, groundLayer);

        if (grounded)
        {
            canDoubleJump = false;
            hasDoubleJumped = false;
        }
        else
        {
            if (!hasDoubleJumped && !holdingJump)
            {
                canDoubleJump = true;
            }
        }

        if (transform.position.y < deathY)
        {
            Die();
        }
    }

    public void TryMove(float xMove)
    {
            Move(xMove);
    }

    private void Move(float xMove)
    {
        if (stunned)
        {
            return;
        }

        if (Mathf.Abs(rigidbody2d.velocity.x) < stats.MaxMoveSpeed)
        {
            float force = xMove * stats.MoveSpeed;
            rigidbody2d.AddForce(new Vector2(force, 0));
            view.SetVelocity(force);
        }
    }

    public void TryStartJump()
    {
        holdingJump = true;
        if (stunned)
        {
            return;
        }

        if (grounded)
        {
            FirstJump();
        }
        else if (canDoubleJump)
        {
            DoubleJump();
        }
    }

    public void TryStopJump()
    {
        holdingJump = false;
        if (jumping)
        {
            jumping = false;
            canDoubleJump = true;
        }
    }

    private void Jump()
    {
        view.Jump();
        Vector3 myVel = rigidbody2d.velocity;
        myVel.y = stats.JumpForce;
        rigidbody2d.velocity = myVel;
        rigidbody2d.AddForce(new Vector2(0, stats.JumpForce));
    }

    private void FirstJump()
    {
        Debug.Log("Jump");

        Jump();
        jumping = true;

    }

    private void DoubleJump()
    {
        Debug.Log("DoubleJump");

        hasDoubleJumped = true;
        canDoubleJump = false;
        Jump();
    }

    public void TryAttack()
    {
        if (stunned)
        {
            return;
        }
        if (!grounded)
        {
            if (airslamAvailableTime <= Time.timeSinceLevelLoad)
            {
                Airslam();
            }
        }
        else if (myController.IsHoldingDown)
        {
            if (uppercutAvailableTime <= Time.timeSinceLevelLoad)
            {
                Uppercut();
            }
        }
        else
        {
            if (punchAvailableTime <= Time.timeSinceLevelLoad)
            {
                Punch();
            }
        }
    }

    private void Airslam()
    {
        currentAttackDecrements = stats.AirslamDamage;
        attackVelocity = stats.AirslamVelocity;
        view.Airslam();
        airslamAvailableTime = Time.timeSinceLevelLoad + stats.AirslamCooldown;
    }

    private void Punch()
    {
        currentAttackDecrements = stats.PunchDamage;
        attackVelocity = stats.PunchVelocity;
        view.Punch();
        punchAvailableTime = Time.timeSinceLevelLoad + stats.PunchCooldown;
    }

    private void Uppercut()
    {
        currentAttackDecrements = stats.UppercutDamage;
        attackVelocity = stats.UppercutVelocity;
        view.Uppercut();
        uppercutAvailableTime = Time.timeSinceLevelLoad + stats.UppercutCooldown;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if ((1<<collision.gameObject.layer & enemyLayer) != 0 && !collision.isTrigger)
        {
            FighterModel enemyModel = collision.gameObject.GetComponentInParent<FighterModel>();
            DealDamage(enemyModel, collision.transform.position.x > transform.position.x);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "pickup" && collision.gameObject != null)
        {
            IncreaseMass(1);
            Destroy(collision.gameObject);
            
        }
    }

    void DecreaseMass(int decrements)
    {
        float scale = Mathf.Max(stats.MinScale, scaleTransform.localScale.x - decrements * stats.ScaleIncrementSize);
        scaleTransform.localScale = new Vector3(scale, scale, scale);
        rigidbody2d.mass = Mathf.Max(stats.MinMass, rigidbody2d.mass - stats.MassIncrementSize * decrements);
        CheckSuper();
    }

    public void IncreaseMass(int increments)
    {
        float scale = Mathf.Min(stats.MaxScale, scaleTransform.localScale.x + increments * stats.ScaleIncrementSize);
        scaleTransform.localScale = new Vector3(scale, scale, scale);
        rigidbody2d.mass = Mathf.Min(stats.MaxMass, rigidbody2d.mass + stats.MassIncrementSize * increments);
        CheckSuper();
    }

    private void DealDamage(FighterModel enemyModel, bool facingRight)
    {
        Vector2 force = attackVelocity;
        if (super)
        {
            force = new Vector2(force.x + stats.SuperBonus, force.y + stats.SuperBonus);
        }
        if (!facingRight)
        {
            force.x = -force.x;
        }

        enemyModel.GetHit(this, force, currentAttackDecrements);
    }

    public void GetHit(FighterModel damagerModel, Vector2 force, int massDecrements)
    {
        view.GetHit();
        rigidbody2d.AddForce(force);

        for (int i = 0; i < massDecrements; i++)
        {
            EjectMass();
        }

        recoverTime = Time.deltaTime + stats.StunTime;
    }

    private void EjectMass()
    {
        DecreaseMass(1);
        Vector3 location = transform.position;
        location.y += 2;
        PickupController pickup = Instantiate(pickupPrefab, location, transform.rotation).GetComponent<PickupController>();
        pickup.SetColor(color.material);
    }

    private void Die()
    {
        view.Die();
        gameplayController.OnFighterLose(this);
        myController.enabled = false;
    }

    void CheckSuper()
    {
        if (super)
        {
            if (rigidbody2d.mass < stats.SuperThreshold)
            {
                super = false;
                view.Super(false);
            }
        }
        else
        {
            if (rigidbody2d.mass >= stats.SuperThreshold)
            {
                super = true;
                view.Super(true);
            }
        }
    }
}