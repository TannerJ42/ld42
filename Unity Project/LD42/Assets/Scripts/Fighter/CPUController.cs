﻿using System.Collections;
using UnityEngine;

public class CPUController : FighterControllerBase
{
    FighterModel myModel;
    FighterModel enemyModel;

    public bool suckerpunch = true;
    public bool bunnyhop = true;
    public bool approachPlayer = true;

    public float attackQuickness = 0.1f;

    void Start()
    {
        myModel = GetComponentInChildren<FighterModel>();
        FighterModel[] fighters = FindObjectsOfType<FighterModel>();

        foreach (var fighter in fighters)
        {
            if (fighter != myModel)
            {
                if (enemyModel != null)
                {
                    Debug.LogWarning("Found more than one enemy.");
                }
                enemyModel = fighter;
            }
        }

        if (enemyModel == null)
        {
            Debug.LogWarning("CPU couldn't find enemy fighter.");
        }

        StartCoroutine(BunnyHop());
    }

    void Update()
    {
        float dist = distanceToEnemy();

        if (approachPlayer && dist > 1.5)
        {
            myModel.TryMove(myModel.transform.position.x < enemyModel.transform.position.x ? 1 : -1);
        }

        if (suckerpunch && distanceToEnemy() < 2.0)
        {
            StartCoroutine(Windup());
            suckerpunch = false;
        }
    }

    private IEnumerator Windup()
    {
        yield return new WaitForSeconds(attackQuickness);
        suckerpunch = true;
        if (distanceToEnemy() < 2.0)
        {
            myModel.TryAttack();
        }
    }

    private IEnumerator BunnyHop()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (bunnyhop && Random.Range(0, 2) == 0)
            {
                myModel.TryStartJump();
                yield return new WaitForSeconds(0.1f);
                myModel.TryStopJump();
            }
        }
    }

    private float distanceToEnemy()
    {
        return Vector2.Distance(myModel.transform.position, enemyModel.transform.position);
    }

    public override bool IsHoldingDown
    {
        get
        {
            return Random.Range(0, 2) == 0;
        }

    }
}
