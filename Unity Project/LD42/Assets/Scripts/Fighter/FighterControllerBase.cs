﻿using UnityEngine;
using System.Collections;

public class FighterControllerBase : MonoBehaviour
{
    public virtual bool IsHoldingDown { get; }
}
