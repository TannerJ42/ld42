﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    public Sprite[] sprites;

    public float delay = 1.0f;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void SetColor(Material color)
    {
        spriteRenderer.material = color;
    }

    void Start()
    {
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
        GetComponent<Rigidbody2D>().AddForce(RandomForce());
        StartCoroutine(Appear());
    }

    Vector2 RandomForce()
    {
        return new Vector2(0, 20);
    }

    IEnumerator Appear()
    {
        yield return new WaitForSeconds(delay);
        GetComponent<BoxCollider2D>().tag = "pickup";
    }
}
